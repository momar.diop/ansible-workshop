# Lab 4
## Objectif 

Utiliser les variables pour optimiser la lecture et l’exécution
Objectif 
À l’aide d’un playbook Ansible :

- Créer le playbook debug.yaml (comme indiquez en page 72) et exécutez le. Prenez le temps d’explorer le résultat. 
- Copier votre playbook du lab3 en lab4
- Editez votre playbook 
  - ajoutez et définissez les variables suivants  :
    - Variables pour identifier la configuration SELINUX
    - Variable pour identifier l’état d’installation du paquetage  
    - Ajoutez l’affichage de la variable Ansible : ansible_memtotal_mb
- Commettez votre nouveau playbook 
